[![ROS](https://upload.wikimedia.org/wikipedia/commons/b/bb/Ros_logo.svg)](https://gitlab.com/VictorLamoine/rwt_urdf_display_test)

# Dependencies
Install Robot Operating System (ROS) `Kinetic` on your Ubuntu:

http://wiki.ros.org/melodic/Installation/Ubuntu

Also install [wstool](http://wiki.ros.org/wstool).

# Catkin workspace
Create a catkin workspace thanks to `wstool`:

```bash
git clone https://gitlab.com/VictorLamoine/rwt_urdf_display_test.git
```

```bash
wstool init src rwt_urdf_display_test/rwt_urdf_display_test.rosinstall
mv rwt_urdf_display_test src
```

# Compile
Use `catkin_make` or `catkin build` to compile the whole workspace.
Make sure the compilation is successful (reaching 100%), if yes the catkin workspace should look like (with `catkin_make`):

# Usage

```bash
roslaunch rwt_urdf_display_test monkeys.launch
```

:information_source: The meshes are hosted at http://victorlamoine.hopto.org/robot_operating_system/rwt_urdf_display_test/

